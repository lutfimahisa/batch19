// var personArr = ["John", "Doe", "male", 27]
// var personObj = {
//     firstName : "John",
//     lastName: "Doe",
//     gender: "male",
//     age: 27
// }
 
// console.log(personArr[0]) // John
// console.log(personObj.firstName) // John 
console.log("soal 1")
function arrayToObject(input)
{
    if (input.length <= 0)
    {
        return console.log("");
    }
    var currentTime = new Date();
    var year = currentTime.getFullYear();

    for (var i = 0; i < input.length; i++)
    {
        var personList = {
            firstName : input[i][0],
            lastName : input[i][1],
            gender : input[i][2],
            age : (input[i][3] && year - input[i][3] > 0? year - input[i][3] : "invalid birth year")
        }
        var output = i+1 + ". " + personList.firstName + " " + personList.lastName + " : ";
        // " : { " + "firstName: \"" + personList.firstName + 
        // "\", lastName : \"" + personList.lastName + 
        // "\", gender : \"" + personList.gender + 
        // "\", age : " + personList.age + " }";
        console.log(output);
        console.log(personList);
    }
}

var input = [["Abduh", "Muhamad", "male", 1992],
             ["Ahmad", "Taufik", "male", 1985]];
arrayToObject(input);

var input2 = [];
arrayToObject(input);

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

console.log(" ")
console.log("soal 2")
function shoppingTime(memberId, money)
{
    if (!memberId)
    {
        return "Mohon Maaf, toko X hanya berlaku member saja";
    }
    if (money < 50000)
    {
        return "Mohon maaf, uang tidak cukup";
    }
    var listProduct = {
        1 : {
            product : "Sepatu",
            brand : "Stacattu",
            harga : 1500000
        },
        2 : {
            product : "Baju",
            brand : "Zorro",
            harga : 500000
        },
        3 : {
            product : "Baju",
            brand : "H&N",
            harga : 250000        
        },
        4 : {
            product : "Sweater",
            brand : "Uniklooh",
            harga : 175000
        },
        5 : {
            product : "Aksesoris",
            brand : "Casing Handphone",
            harga : 50000
        }
    }
    var orderedPrice = Object.keys(listProduct).sort(function (keyA, keyB){
        return listProduct[keyB].harga - listProduct[keyA].harga
    });
    var sisaUang = money;
    var beli = {
        memberId : memberId,
        money : money,
        listPurchased : [],
        changeMoney : 0
    };
    for(var i = 0; i < orderedPrice.length; i++)
    {
        if ( sisaUang - listProduct[orderedPrice[i]].harga >= 0 )
        {
            sisaUang -= listProduct[orderedPrice[i]].harga;
            beli["listPurchased"].push(listProduct[orderedPrice[i]].product + " " + listProduct[orderedPrice[i]].brand);
            beli["changeMoney"] = sisaUang;
        }
    }
    return beli;
}

console.log(shoppingTime("", 2475000));
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log(" ")
console.log("soal 3")
function naikAngkot(input) //using multidimensional object
{
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var listPenumpang = {};
    for (var i = 0; i < input.length; i++)
    {
        listPenumpang[i.toString()] = {
            penumpang : input[i][0],
            naikDari : input[i][1],
            tujuan : input[i][2]
        }
    }
    return listPenumpang;
}

function naikAngkot2(input) //using array of objects
{
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var listPenumpang = [];
    for (var i = 0; i <= input.length; i++)
    {
        var newObject = {};
        newObject.penumpang = input[i][0];
        newObject.naikDari = input[i][1];
        newObject.tujuan = input[i][2];
        listPenumpang.push(newObject);
    }
    return listPenumpang;
}

var penumpang = [["Dimitri", "B", "F"], 
                 ["Icha", "A", "B"]];
console.log(naikAngkot(penumpang));

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); //[]

console.log(naikAngkot2(penumpang));
console.log(naikAngkot2([])); //[]
