IndonesiaMengoding

InfoQuiz

Silahkan buat folder baru seperti pada pengerjaan tugas dengan nama "Quiz3"

di dalam folder project Expo/React Native Anda.

Masukkan file index.js, LoginScreen.js, HomeScreen.js, data.json ke dalam folder tersebut

Import / panggil index.js di App.js pada root folder project agar bisa ditampilkan pada device/emulator Anda

Quiz kali ini terdiri dari 3 Soal Utama dan 2 Soal Bonus (Tambahan Soal 1 dan Soal Total Harga)

Penjelasan dan lokasi soal dapat dicari pada file LoginScreen.js dan HomeScreen.js

dengan menggunakan fitur search "#Soal" atau "//?" tanpa petik (")

Berikut daftar soal pada Quiz kali ini




Contoh hasil akhir dari aplikasi dapat dilihat pada link berikut (jenis file .gif):

https://www.youtube.com/watch?v=d3jIts_sgOc&feature=youtu.be


Setelah selesai mengerjakan, push pekerjaan Anda ke repository gitlab masing-masing

Input link commit hasil pekerjaan Anda ke sanbercode.com

Kerjakan soal semampunya dengan baik dan jujur

Tidak perlu bertanya kepada trainer terkait soal, cukup dikerjakan sesuai pemahaman peserta

Peserta diperbolehkan untuk googling, namun tidak diperbolehkan untuk bertanya, berdiskusi, atau mencontek.

Selama quiz berlangsung, grup diskusi telegram akan dinonaktifkan (peserta tidak dapat mengirimkan pesan atau berdiskusi di grup)

Selamat mengerjakan