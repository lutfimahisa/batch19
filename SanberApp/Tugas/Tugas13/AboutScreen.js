import React, { useState } from 'react'
import { 
    View, 
    Image, 
    StyleSheet, 
    TouchableOpacity, 
    Text,
    Platform, 
    ScrollView,
    KeyboardAvoidingView,
    CheckBox} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';

const AboutScreen = () => {
    const [isSelected, setSelection] = useState(false);
    return (
        <KeyboardAvoidingView
        behavior ={Platform.OS == "ios" ? "padding" : "height"}
        style={styles.container}
        >
        <ScrollView>
        <View style={styles.boxLogout}>
                    <TouchableOpacity style={styles.btLogout}>
                        <Text style={styles.textLogout}>LOGOUT</Text>
                    </TouchableOpacity>
                </View>
        <View style={styles.containerView}>
            <View style={styles.header}>
                <Image style={styles.foto} source={require('./images/foto.png')}/>
            </View>
            
            <View style={styles.content}>
                <View style={styles.firstbox} >
                    
                    <Text style={styles.myName}>Lutfi Mahisa</Text>
                    <View style={styles.boxElement}>
                        <Icon style={styles.sosmed} name="home" size={15} />
                        <Text>Jakarta</Text>
                    </View>
                    
                    <View style={styles.boxElement}>
                        <Icon  style={styles.sosmed} name="call" size={15} />
                        <Text>08111323242</Text>
                    </View>
                    
                </View>

                <View style={styles.box} >
                    <Text style={styles.myName}>My Portfolio</Text>
                    <View style={styles.boxBetween}>
                        <TouchableOpacity style={styles.boxElement}>
                            <Image style={styles.sosmed} source={require('./images/gitlab.jpg')}/>
                            <Text style={{textDecorationLine: 'underline'}}>@lutfimahisa</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.boxElement}>
                            <Image style={styles.sosmed} source={require('./images/ig.png')}/>
                            <Text style={{textDecorationLine: 'underline'}}>@lutfimahisa</Text>
                        </TouchableOpacity>
                        {/* <TouchableOpacity style={styles.boxElement}>
                            <Image style={styles.sosmed} source={require('./images/tw.png')}/>
                            <Text style={{textDecorationLine: 'underline'}}>@jumatsidik</Text>
                        </TouchableOpacity> */}
                    </View>
                </View>

                

                
            </View>
        </View>
        </ScrollView>
        </KeyboardAvoidingView>
    )
}

export default AboutScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#eee'
    },
    containerView: {
       
    },
    header: {
        height: 270,
        backgroundColor: '#04BEC7',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
    foto: {
        flex: 1,
        width: 600,
    },

    Content: {
        
    },
    firstbox: {
        backgroundColor: '#fff',
        marginHorizontal: 40,
        marginBottom: 20,
        marginTop: -20,
        padding: 20,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    box: {
        backgroundColor: '#fff',
        marginHorizontal: 40,
        marginBottom: 20,
        padding: 20,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    howday: {
        fontSize: 15,
        fontWeight: '400',
        color: '#777',
    },
    myName: {
        fontSize: 30,
        fontWeight: '700',
        color: '#04BEC7',
    },
    boxElement: {
        flexDirection: 'row',
        marginVertical: 3,

    },
    boxBetween: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    sosmed: {
        width: 20,
        height: 20,
        marginRight: 5,
    },
    boxflexStart: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    sideElement: {
        marginRight: 20,
    }, 
    boxLogout: {
        marginHorizontal: 40,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    textLogout: {
        fontWeight: '700',
        color: '#04BEC7',
    },
    btLogout: {
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        marginBottom: 20,
    },

  })
