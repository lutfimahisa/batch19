//Soal 1
var nama = "Juna"
var peran = "Werewolfzz"
if ( nama == "" ) {
    console.log("nama harus di isi!")
} else if( nama == "John" ) {
    console.log("Halo John, Pilih peranmu untuk memulai game!")
} else if( nama == "Jane" || peran == "Penyihir") {
    console.log("Selamat datang di Dunia Werewolf, Jane")
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
} else if(nama == "Jenita" || peran == "Guard"){
    console.log("Selamat datang di Dunia Werewolf, Jenita")
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf")
} else if(nama == "Junaedi" || peran == "Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, Junaedi")
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
} else {
    console.log("Silahkan masukkan nama dan peran dengan benar!")
}

//Soal 2
var tanggal = 1;
var bulan = 2;
var tahun = 2;
switch(tanggal) {
  case 1:   { console.log('1'); break; }
  case 2:   { console.log('2'); break; }
  case 3:   { console.log('3'); break; }
  case 4:   { console.log('4'); break; }
  default:  { console.log('5'); }}
switch(bulan) {
    case 1:   { console.log('jan'); break; }
    case 2:   { console.log('feb'); break; }
    case 3:   { console.log('mar'); break; }
    case 4:   { console.log('may'); break; }
    default:  { console.log('apr'); }}

switch(tahun) {
        case 1:   { console.log('jan'); break; }
        case 2:   { console.log('2020'); break; }
        case 3:   { console.log('mar'); break; }
        case 4:   { console.log('may'); break; }
        default:  { console.log('apr'); }}

        //clueless