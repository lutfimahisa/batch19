// var hobbies = ["coding", "cycling", "climbing", "skateboarding"] 
// console.log(hobbies) // [ 'coding', 'cycling', 'climbing', 'skateboarding' ]
// console.log(hobbies.length) // 4 
 
// console.log(hobbies[0]) // coding
// console.log(hobbies[2]) // climbing
// // Mengakses elemen terakhir dari array
// console.log(hobbies[hobbies.length -1]) // skateboarding
// Range(startNum, FinishNum)
// var angka = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60]
// var irisan1 = angka.slice(1,10) 
// console.log(irisan1) //[1, 2, 3]
// var irisan2 = angka.slice(0,2)
// console.log(irisan2) //[0, 1, 2] 

// console.log("soal 1")
// function range(startnum, finishnum){
//     var rangearray =[];
//     if( startnum  > finishnum){
//         var rangelength = startnum - finishnum + 1;
//         for (var i = 0; i< rangelength; i++){
//             rangearray.push(startnum - i)
//         }
//     } else if (startnum < finishnum){
//         var rangelength = finishnum - startnum + 1;
//         for (var i = 0; i < rangelength; i++){
//             rangearray.push(startnum + i)
//         }
//     } else if (!startnum || ! finishnum){
//         return -1
//     }
//     return rangearray
// }
// console.log(range(1, 10))
// console.log(range(1))
// console.log(range(11, 18))
// console.log(range(54, 50))
// console.log(range())

// function rangewithstep(startnum, finishnum, step){
//     var rangearray =[];
//     if( startnum  > finishnum){
//         var currentnum = startnum ;
//         for (var i = 0; currentnum >= finishnum; i++){
//             rangearray.push(currentnum)
//             currentnum -= step
//         }
//     } else if (startnum < finishnum){
//         var currentnum = startnum ;
//         for (var i = 0; currentnum <= finishnum; i++){
//             rangearray.push(currentnum)
//             currentnum += step
//         }
        
//     } else if (!startnum || ! finishnum || !step){
//         return -1
//     }
//     return rangearray
// }
// console.log(rangewithstep(1, 10, 2))
// console.log(rangewithstep(11, 23, 3))
// console.log(rangewithstep(5, 2, 1))
// console.log(rangewithstep(29, 2, 4))

// function sumOfRange(numbers) {
//     var sum = 0;
//     for (var i = 0; i < numbers.length; i++) {
//         sum += numbers[i];
//     }
//     return sum;
// }
// console.log(sumOfRange([1, 10]));
// console.log(sumOfRange([1,2,3,4,5]));
// console.log(sumOfRange([-4,-5,-10,0]));

function myApp(){
    var total = 6;
    var output = "#";
    for (var i =  1; i<=total; i++){
        for (var j = 1; j<= i; j++){
            output +=j + "#";
        }
        console.log(output);
        output="#";
    }
}

myApp();