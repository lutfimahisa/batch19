console.log("");
console.log("Soal 1")
goldenFunction = () => {
    console.log("this is golden!!")
  }
   
  goldenFunction()


console.log("");
console.log("Soal 2")
const newFunctionEs6 = (firstName, lastName) =>{
    return {
      firstName,
      lastName,
      fullName(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
  newFunctionEs6("William", "Imoh").fullName();
  console.log(newFunctionEs6("William", "Imoh").firstName);

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const firstName = newObject.firstName;
const lastName = newObject.lastName;
const destination = newObject.destination;
const occupation = newObject.occupation;

console.log(firstName, lastName, destination, occupation);


console.log("");
console.log("Soal 3")
const newObject1 = {
    firstName1: "Harry",
    lastName1: "Potter Holt",
    destination1: "Hogwarts React Conf",
    occupation1: "Deve-wizard Avocado",
    spell1: "Vimulus Renderus!!!"
}
const { firstName1, lastName1, destination1, occupation1, spell1 } = newObject1;

console.log(firstName1, lastName1, destination1, occupation1);

console.log("");
console.log("Soal 4")
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];


let combined1 = [...west, ...east];
console.log(combined1);


console.log("");
console.log("Soal 5")
const planet = 'earth'
const view = 'glass'

 
const theString = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
console.log(theString)

// const planet = "earth"
// const view = "glass"
// var before = 'Lorem ' + view + 'dolor sit amet, ' +  
//     'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam'
 
// // Driver Code
// console.log(before) 